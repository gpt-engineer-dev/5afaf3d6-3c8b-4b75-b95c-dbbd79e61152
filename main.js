const video = document.createElement("video");
video.setAttribute("autoplay", "");
video.setAttribute("muted", "");
video.setAttribute("playsinline", "");

const cameraView = document.getElementById("camera-view");
if (cameraView) {
  cameraView.appendChild(video);
}

if (location.protocol !== "https:") {
  const errorMessage = document.createElement("p");
  errorMessage.textContent = "Camera access requires HTTPS. Please ensure you're accessing the page over a secure connection.";
  errorMessage.style.color = "red";
  cameraView.appendChild(errorMessage);
} else {
  navigator.mediaDevices
    .getUserMedia({ video: true })
    .then((stream) => {
      video.srcObject = stream;
    })
    .catch((error) => {
      console.error("Error accessing camera:", error);
      const errorMessage = document.createElement("p");
      errorMessage.textContent = "Error accessing camera: Permission denied. Please check your browser settings and ensure camera access is allowed.";
      errorMessage.style.color = "red";
      cameraView.appendChild(errorMessage);
    });
}

const shutterButton = document.getElementById("shutter-button");
shutterButton.addEventListener("click", () => {
  const canvas = document.createElement("canvas");
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  canvas.getContext("2d").drawImage(video, 0, 0);
  const imgDataUrl = canvas.toDataURL("image/jpeg");

  // Store the captured image in local storage
  let images = JSON.parse(localStorage.getItem("images")) || [];
  images.push(imgDataUrl);
  localStorage.setItem("images", JSON.stringify(images));

  // Create a new anchor element and use it to download the image
  const downloadLink = document.createElement("a");
  downloadLink.href = imgDataUrl;
  downloadLink.download = "captured_photo.jpeg";
  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);

  console.log("Captured and downloaded photo:", imgDataUrl);
});

// Display the images in the gallery
const gallery = document.getElementById("gallery");
if (gallery) {
  const images = JSON.parse(localStorage.getItem("images")) || [];
  images.forEach((imgDataUrl) => {
    const img = document.createElement("img");
    img.src = imgDataUrl;
    gallery.appendChild(img);
  });
}
